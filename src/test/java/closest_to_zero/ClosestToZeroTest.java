package closest_to_zero;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ClosestToZeroTest {
	
	@Test
	public void emptyArrayTest() {
		int[] testValues = new int[] {};
		int result = ClosestToZero.retrieveClosestToZero(testValues);
		assertEquals(0, result);
	}
	
	@Test
	public void undefinedArrayTest() {
		int[] testValues = null;
		int result = ClosestToZero.retrieveClosestToZero(testValues);
		assertEquals(0, result);
	}
	
	@ParameterizedTest
	@ValueSource(ints = {0, -1, 9, -8})
	public void singleValueTest(int singleValue) {
		int[] testValues = new int[] {singleValue};
		int result = ClosestToZero.retrieveClosestToZero(testValues);
		assertEquals(singleValue, result);
	}
	
	@Test
	public void positiveOverNegativeTest() {
		int[] testValues = new int[] {3, 4, -3, 80, -1, 1};
		int result = ClosestToZero.retrieveClosestToZero(testValues);
		assertEquals(1, result);
	}
	
	@Test
	public void containsZeroTest() {
		int[] testValues = new int[] {0, -1, 9, -8, 8, 9, 1};
		int result = ClosestToZero.retrieveClosestToZero(testValues);
		assertEquals(0, result);
	}
}
