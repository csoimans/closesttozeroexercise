package closest_to_zero;

public class ClosestToZero {

	/**
	 * This function retrieves the closest int to 0 in an array of int. Positive
	 * closest value will be retained over negative one. Empty array or null element
	 * will return 0.
	 * 
	 * Personal comment: specifications should not ask to return 0 for empty arrays
	 * or null arrays too. Exception would be better.
	 */
	public static int retrieveClosestToZero(int[] values) {

		// Array is not defined or empty
		if (values == null || values.length == 0) {
			return 0;
		}

		/*
		 * Since there is no explicit limit, I'll use JVM limitations
		 * to decide for biggest value possible.
		 * */
		int closestValue = Integer.MAX_VALUE;
		int currentDiff = Integer.MAX_VALUE;
		int index = 0;

		
		/*
		 * If array contains 0, it is the best answer, otherwise, look for smallest.
		 * */
		
		do {
			currentDiff = Math.abs(values[index]);
			if (currentDiff < closestValue || (currentDiff == Math.abs(closestValue) && closestValue < 0)) {
				closestValue = values[index];
			}
			index++;
		} while (closestValue != 0 && (index < values.length));

		return closestValue;
	}

	public static void main(String[] args) {
		// JUnit tests are available to assert the code is compliant with requirements.
		int[] testValues = new int[] {7,3, 903, -6, -3};
		System.out.println(retrieveClosestToZero(testValues));
	}

}
